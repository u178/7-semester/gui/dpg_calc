import dearpygui.dearpygui as dpg
from interpreter import Interpreter

btn = ["123+", "456-", "789*", "C0=/"]

dpg.create_context()

whitelist = '0123456789-+/* =^'
interpreter = Interpreter()


def button_handler(sender, _, user_data):
    # banned_chars = ''.join(filter(lambda x: x not in whitelist, user_data))
    if user_data == '=':
        try:
            result = interpreter.interpret_line(dpg.get_value('__input_text'))
            dpg.set_value("__input_text", f"{result:g}")
        except:
            dpg.set_value("__input_text", "")

    elif user_data == 'C':
        dpg.set_value("__input_text", "")
    else:
        dpg.set_value("__input_text", f"{dpg.get_value('__input_text') + user_data}")


input_text = ""

with dpg.window(label="OMG CALC"):

    dpg.add_input_text(indent=0, decimal=True, no_spaces=True, tag="__input_text")
    # dpg.show_style_editor()
    with dpg.table(header_row=False, resizable=True, policy=dpg.mvTable_SizingStretchProp,
                   borders_outerH=True, borders_innerV=True, borders_outerV=True):

        for i in range(4):
            dpg.add_table_column()

        for i in range(4):
            with dpg.table_row():
                for j in range(4):
                    dpg.add_button(label=f"{btn[i][j]}", callback=button_handler, user_data=f"{btn[i][j]}")


dpg.create_viewport(title='AMG CALC!!!', width=800, height=600)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()
