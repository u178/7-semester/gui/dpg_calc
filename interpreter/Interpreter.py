from .Tokens import TokenType
from .Node import BinOp, Node, Number, UnaryOp, ASTList, NoOp
import operator
from .Parcer import Parser


class InterpreterException(Exception):
    pass


class Interpreter:

    def __init__(self):
        self.d = None

    def __str__(self):
        return f"interpreter. data: {self.d}"

    def interpret(self, tree: Node) -> dict:
        return self._visit(tree)

    def interpret_line(self, equation: str):
        p = Parser()
        return self.interpret(p.parse(equation))

    def _visit(self, node: Node):

        if isinstance(node, ASTList):
            return self._visit_ASTLIST(node)
        if isinstance(node, NoOp):
            return self._visit_noop(node)
        if isinstance(node, UnaryOp):
            return self._visit_unop(node)
        if isinstance(node, Number):
            return self._visit_number(node)
        if isinstance(node, BinOp):
            return self._visit_binop(node)

        #  currently, unreachable. In case of adding new Nodes or breaking Parser
        raise InterpreterException("invalid node")


    def _visit_number(self, node: Node) -> float:
        return float(node.token.value)

    def _visit_noop(self, node):
        pass

    def _visit_binop(self, node: Node):
        op = node.op

        binop = {TokenType.PLUS: operator.add,
                 TokenType.MINUS: operator.sub,
                 TokenType.DIV: operator.truediv,
                 TokenType.MUL: operator.mul,
                 TokenType.POW: operator.pow}.get(op.type_)

        if binop:
            return binop(self._visit(node.left), self._visit(node.right))

    def _visit_unop(self, node: Node):
        op = node.op

        if op.type_ == TokenType.PLUS:
            return self._visit(node.node)
        if op.type_ == TokenType.MINUS:
            return -self._visit(node.node)
        raise InterpreterException("invalid operator")


